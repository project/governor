<?php

namespace Drupal\governor;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Theme\StarterKitInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

final class StarterKit implements StarterKitInterface {

  /**
   * Array of info file overrides.
   *
   * @var array
   */
  private static $infoOverrides = [];

  /**
   * List of files & directories that shouldn't be copied over.
   *
   * @var array
   */
  private static $pathsToDelete = [];

  /**
   * Array of files to avoid renaming.
   *
   * @var array
   */
  private static $pathsToSkipRename = [];

  /**
   * Array of files to avoid editing.
   *
   * @var array
   */
  private static $pathsToSkipEdit = [];

  /**
   * Reads THEMENAME.starterkit.yml.
   *
   * @return void
   */
  private static function getStarterKitConfig($working_dir, $machine_name) {
    if ($config_file = file_get_contents($working_dir . '/' . $machine_name . '.starterkit.yml')) {
      $config = Yaml::decode($config_file);

      if (!empty($config['delete'])) {
        self::$pathsToDelete = $config['delete'];
      }

      if (!empty($config['no_edit'])) {
        self::$pathsToSkipEdit = $config['no_edit'];
      }

      if (!empty($config['no_rename'])) {
        self::$pathsToSkipRename = $config['no_rename'];
      }

      if (isset($config['info']) && is_array($config['info'])) {
        self::$infoOverrides = $config['info'];
      }
    }
  }


  /**
   * Finds and replaces string/regex matches in file names and contents.
   *
   * @param string $dir
   *   The working directory of the template being generated.
   * @param string $find
   *   The string to be removed.
   * @param string $replace
   *   The string to be added.
   * @param bool $skip_filters
   *   If `true`, do not filter results based on $pathsToSkipRename or $pathsToSkipEdit arrays.
   */
  private static function findAndReplace($dir, $find, $replace, $skip_filters = FALSE): void {
    $fs = new Filesystem();

    // Edit file names.
    $finder = new Finder();
    $finder->files()
      ->in($dir)
      ->name("/$find/")
      ->filter(function (\SplFileInfo $file) use ($dir, $skip_filters) {
        if (!$skip_filters) {
          $relative_path = str_replace($dir . '/', '', $file->getPathname());
          return !in_array($relative_path, self::$pathsToSkipRename);
        }
        else {
          return TRUE;
        }
      });
    foreach ($finder as $file) {
      $filepath_segments = explode('/', $file->getRealPath());
      $filename = array_pop($filepath_segments);
      $filename = str_replace($find, $replace, $filename);
      $filepath_segments[] = $filename;
      $fs->rename($file->getRealPath(), implode('/', $filepath_segments));
    }

    // Edit file contents.
    $finder = new Finder();
    $finder->files()
      ->in($dir)
      ->contains($find)
      ->filter(function (\SplFileInfo $file) use ($dir, $skip_filters) {
        if (!$skip_filters) {
          $relative_path = str_replace($dir . '/', '', $file->getPathname());
          return !in_array($relative_path, self::$pathsToSkipEdit);
        }
        else {
          return TRUE;
        }
      });
    foreach ($finder as $file) {
      $contents = file_get_contents($file->getRealPath());
      $contents = str_replace($find, $replace, $contents);
      file_put_contents($file->getRealPath(), $contents);
    }
  }

  /**
   * Updates values in the new theme's .info.yml file.
   *
   * @param string $working_dir
   *   The working directory of the template being generated.
   * @param string $machine_name
   *   The theme's machine name.
   * @param string $theme_name
   *   The theme's name.
   */
  private static function updateThemeInfo(string $working_dir, string $machine_name, string $theme_name): void {
    $info_overrides = self::$infoOverrides;
    $info_file = "$working_dir/$machine_name.info.yml";
    $info = Yaml::decode(file_get_contents($info_file));
    if (isset($info_overrides) && is_array($info_overrides) && !empty($info_overrides)) {
      foreach ($info_overrides as $key => $value) {
        if ($value === NULL) {
          unset($info[$key]);
        }
        else {
          $info[$key] = $value;
        }
      }
    }
    file_put_contents($info_file, Yaml::encode($info));
  }

  /**
   * Removes $pathsToDelete files & directories from the working directory prior to copying into final destination.
   *
   * @param string $working_dir
   *   The working directory of the template being generated.
   */
  private static function removeDeletableFiles(string $working_dir, $machine_name): void {
    $fs = new Filesystem();

    foreach (self::$pathsToDelete as $item) {
      $fs->remove($working_dir . $item);
    }

    // Delete starterkit.yml file.
    $fs->remove("$working_dir/$machine_name.starterkit.yml");
  }

  /**
   * {@inheritdoc}
   */
  public static function postProcess(string $working_dir, string $machine_name, string $theme_name): void {

    self::getStarterKitConfig($working_dir, $machine_name);

    self::updateThemeInfo($working_dir, $machine_name, $theme_name);

    self::removeDeletableFiles($working_dir, $machine_name);

    self::findAndReplace($working_dir, 'governor', $machine_name);
    self::findAndReplace($working_dir, 'The Governor', $theme_name);
    self::findAndReplace($working_dir, 'starterkit.md', 'README.md', TRUE);
  }

}
