# The Governor Starterkit Theme

## How to use The Governor Starterkit

To generate a new theme from The Governor using the starterkit/theme-generation
script, run the following from Drupal's installation root:

```sh
composer require symfony/filesystem
composer require symfony/finder
php core/scripts/drupal generate-theme new_theme_name --starterkit governor
```

Additionally, you can create the theme's human-readable name and it description
with two optional arguments:

```sh
--name "New Theme Name" --description "Custom theme generated from The Governor theme"
```

This script will copy over all the files from the Governor theme, and replace
instances of Olivero's machine name and label with the strings you provide.

## Getting started

1. Have gulp installed globally
2. From the new theme's directory, run `npm install`
3. Run `gulp init`
4. Update the `$theme-image-path` value within `sass/uswds/_settings.scss` to point to
   your theme's images

## Customizing CSS
